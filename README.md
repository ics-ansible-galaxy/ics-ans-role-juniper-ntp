# ics-ans-role-juniper-ntp

Ansible role to configure juniper switches to use the ntp servers.

## Role Variables

```yaml
juniper_ntp_boot_server: 172.30.0.38
juniper_ntp_server: 172.30.0.38
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-juniper-ntp
```

## License

BSD 2-clause
