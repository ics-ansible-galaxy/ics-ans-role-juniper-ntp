import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_syslog_remote_host_added(host):
    assert os.system("zcat /config/juniper.conf.gz | grep boot-server")
    assert os.system("zcat /config/juniper.conf.gz | grep 172.30.0.38")
